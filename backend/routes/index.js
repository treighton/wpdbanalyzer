var express = require("express");

var router = express.Router();
// edit burger model to match sequelize
var Sequelize = require("sequelize");
var db = require("../models/");
var moment = require("moment");
router.get("/", (req, res) => {
  db.wp_posts
    .findAll({
      where: {
        post_type: "page",
        post_status: { $ne: "publish" },
        post_modified: { $gt: new Date().getFullYear() - 1 }
      }
    })
    .then(result => res.json(result));
});
router.get("/old", (req, res) => {
  console.log(moment().subtract(1, "years"));
  db.wp_posts
    .findAll({
      where: {
        post_type: "page",
        post_title: {
          $like: "%Route%"
        },
        post_parent: 167,
        post_modified: { $gt: moment().subtract(2, "years") }
      }
    })
    .then(result => res.json(result));
});

router.get("/page/:id", (req, res) => {
  const route = req.params.id;
  db.wp_posts
    .findAll({ where: { ID: route } })
    .then(result => res.json(result));
});

module.exports = router;
