# WP Database Analyzer

This is a work in progress, but in theory will be a tool to help developers get some insight into their WP databases.

## Purpose

Wordpress Database Analyzer (**WPDBA**) was built to help me go create a report for a client about stuff in the database that could be cleaned out (and after 6 years, there is quite a bit).

While manually going through the pages and posts would have worked, or using (Sequel Pro)[https://www.sequelpro.com/] to perform queries and export to csv would have worked too, I felt compelled to build a more interactive tool that i could use to create a web based report on the database to send to my client.

Who knows, after this project i may never use it again...

if you want to
